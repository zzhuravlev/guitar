/*--------------------------------------------------*/

#import <UIKit/UIKit.h>

/*--------------------------------------------------*/

IB_DESIGNABLE
@interface SpaceLabel : UILabel

@property(nonatomic, readwrite, assign) IBInspectable CGFloat letterSpacing;
@property(nonatomic, readwrite, assign) IBInspectable CGFloat mLineSpacing;

@end

/*--------------------------------------------------*/
