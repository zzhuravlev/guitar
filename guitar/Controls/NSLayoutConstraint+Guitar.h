/*--------------------------------------------------*/

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

/*--------------------------------------------------*/

@interface NSLayoutConstraint (Guitar)

@property(nonatomic, readwrite, assign) IBInspectable NSInteger preciseConstant;

@end

/*--------------------------------------------------*/
