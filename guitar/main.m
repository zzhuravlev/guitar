//
//  main.m
//  guitar
//
//  Created by Zakhar Zhuravlev on 24/08/15.
//  Copyright (c) 2015 zakhar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
