/*--------------------------------------------------*/

#import "NSLayoutConstraint+Guitar.h"

/*--------------------------------------------------*/

@implementation NSLayoutConstraint (Guitar)

- (NSInteger)preciseConstant {
    return self.constant * UIScreen.mainScreen.scale;
}

- (void)setPreciseConstant:(NSInteger)preciseConstant {
    self.constant = preciseConstant / UIScreen.mainScreen.scale;
}

@end

/*--------------------------------------------------*/
