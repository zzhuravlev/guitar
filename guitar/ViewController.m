//
//  ViewController.m
//  guitar
//
//  Created by Zakhar Zhuravlev on 24/08/15.
//  Copyright (c) 2015 zakhar. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property(nonatomic, readwrite, weak) IBOutlet UIView* contentView;
@property(nonatomic, readwrite, strong) IBOutletCollection(UIButton) NSArray* buttons;
@property(nonatomic, readwrite, strong) IBOutletCollection(UIButton) NSArray* bottomButtons;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    for(UIButton* btn in _buttons) {
        btn.layer.cornerRadius = 5;
        btn.clipsToBounds = YES;
    }
    
    for(UIButton* btn in _bottomButtons) {
        btn.layer.borderColor = [btn.titleLabel.textColor CGColor];
        btn.layer.borderWidth = 1.0f;
        btn.layer.cornerRadius = 5;
        btn.clipsToBounds = YES;
    }
    
    NSLayoutConstraint *leftConstraint = [NSLayoutConstraint constraintWithItem:self.contentView
                                                                      attribute:NSLayoutAttributeLeading
                                                                      relatedBy:NSLayoutRelationEqual
                                                                         toItem:self.view
                                                                      attribute:NSLayoutAttributeLeft
                                                                     multiplier:1.0
                                                                       constant:0];
    [self.view addConstraint:leftConstraint];
    
    NSLayoutConstraint *rightConstraint = [NSLayoutConstraint constraintWithItem:self.contentView
                                                                       attribute:NSLayoutAttributeTrailing
                                                                       relatedBy:NSLayoutRelationEqual
                                                                          toItem:self.view
                                                                       attribute:NSLayoutAttributeRight
                                                                      multiplier:1.0
                                                                        constant:0];
    [self.view addConstraint:rightConstraint];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
