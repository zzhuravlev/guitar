/*--------------------------------------------------*/

#import "SpaceLabel.h"

/*--------------------------------------------------*/

@interface SpaceLabel ()

- (void)_updateWithText:(NSString*)text;

@end

/*--------------------------------------------------*/

@implementation SpaceLabel

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self _updateWithText:self.text];
}

- (void)setText:(NSString*)text {
    if([self.attributedText.string isEqualToString:text] == NO) {
        [self _updateWithText:text];
    }
}

- (void)setAttributedText:(NSAttributedString*)attributedText {
    if([self.attributedText isEqualToAttributedString:attributedText] == NO) {
        [self _updateWithText:attributedText.string];
    }
}

- (void)setLetterSpacing:(CGFloat)letterSpacing {
    if(_letterSpacing != letterSpacing) {
        _letterSpacing = letterSpacing;
        [self _updateWithText:self.attributedText.string];
    }
}

- (void)setMLineSpacing:(CGFloat)mLineSpacing {
    if(_mLineSpacing != mLineSpacing) {
        _mLineSpacing = mLineSpacing;
        [self _updateWithText:self.attributedText.string];
    }
}

- (void)_updateWithText:(NSString*)text {
    if(text.length > 0) {
        NSMutableParagraphStyle *paragrahStyle = [[NSMutableParagraphStyle alloc] init];
        paragrahStyle.lineSpacing = _mLineSpacing;
        paragrahStyle.alignment = self.textAlignment;
        super.attributedText = [[NSAttributedString alloc] initWithString:text attributes:@{
                                                                                            NSFontAttributeName: self.font,
                                                                                            NSForegroundColorAttributeName: self.textColor,
                                                                                            NSKernAttributeName: @(_letterSpacing),
                                                                                            NSParagraphStyleAttributeName: paragrahStyle
                                                                                            }];
    } else {
        super.attributedText = nil;
    }
}

@end

/*--------------------------------------------------*/
